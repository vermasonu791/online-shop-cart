import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import * as firebase from "firebase";
import Notifications from "vue-notification";
import VeeValidate from "vee-validate";

Vue.use(VeeValidate);
Vue.use(Notifications);

Vue.config.productionTip = false;

const config = {
  apiKey: "AIzaSyATS-yRsjXx-zoWrPQjwb9eVv3R4qPYA6Q",
  authDomain: "online-shopping-62f8d.firebaseapp.com",
  databaseURL: "https://online-shopping-62f8d.firebaseio.com",
  projectId: "online-shopping-62f8d",
  storageBucket: "online-shopping-62f8d.appspot.com",
  messagingSenderId: "251856634120"
};

firebase.initializeApp(config);
Vue.prototype.$firebase = firebase;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

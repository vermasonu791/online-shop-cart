import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Admin from "./views/Admin.vue";
import Cart from "./views/Cart.vue";
import View from "./views/Viewproducts.vue";
import Edit from "./views/Editproducts.vue";
import Singleitem from "./views/Viewsingleproduct.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/admin",
      name: "admin",
      component: Admin,
      children: [
        {
          path: "/viewproducts",
          name: "view",
          component: View
        },
        {
          path: "/editproducts",
          name: "edit",
          component: Edit
        }
      ]
    },
    {
      path: "/cart",
      name: "Cart",
      component: Cart
    },
    {
      path: "/single-product/:id",
      name: "single-product",
      component: Singleitem
    }
  ]
});

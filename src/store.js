import Vue from "vue";
import Vuex from "vuex";

import * as firebase from "firebase";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: [],
    cartcount: 0,
    cartItem: [],
    iscartEmpty: true
  },
  mutations: {
    set_product: (state, prod) => {
      state.products = prod;
    },
    addCart: (state, payload) => {
      console.log(payload);
      state.iscartEmpty = false;
      state.cartcount++;
      state.cartItem.push(payload);
      // console.log(state.cartItem);
    },
    removeItemCart: (state, payload) => {
      state.cartcount -= 1;
      state.cartItem.splice(state.cartItem.indexOf(payload), 1);
      if (state.cartcount == 0) {
        state.iscartEmpty = true;
      }
    },
    addnew(state, data) {
      state.products = data;
      console.log(data);
      console.log(typeof state.products);
    },
    deleted(state) {
      state.products;
    },
    edited() {}
  },
  getters: {
    getProduct: state => {
      return state.products;
    },
    getCartCount: state => {
      return state.cartcount;
    },
    getCartItem: state => {
      return state.cartItem;
    },
    getCartEmpty: state => {
      return state.iscartEmpty;
    }
  },
  actions: {
    getProduct({ commit }) {
      var ref = firebase.database().ref("products");
      ref.on("value", function(snapshot) {
        var data = snapshot.val();
        // console.log(data);
        commit("set_product", data);
      });
    },
    addNewdata({ commit }, payload) {
      var ref = firebase.database().ref("products");
      ref.push(payload).then(response => {
        // console.log(response);
        commit("addnew", response);
      });
    },
    EditData({ commit }, payload) {
      // console.log(payload.updateData);
      firebase
        .database()
        .ref("products/" + payload.editkey)
        .set(payload.updateData);
      commit("edited");
    },
    deleteData({ commit }, payload) {
      console.log(payload);
      firebase
        .database()
        .ref("products/" + payload)
        .remove();
      commit("deleted");
    }
  }
});
